from django.shortcuts import render
import datetime

# Create your views here.

from . import models

def index(request):
    person = models.Person.objects.get(pk=1)
    uhrzeit = datetime.datetime.now()
    context = {
        "uhrzeit": uhrzeit,
        "cute_name": person.cute_name()
    }
    return render(request, "uhrzeit/index.html", context)
