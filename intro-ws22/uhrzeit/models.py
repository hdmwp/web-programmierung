from django.db import models

# Create your models here.


class Person(models.Model):
    name = models.TextField()

    def cute_name(self):
        return self.name + "lein"

    def __str__(self):
        return self.name
