from django.apps import AppConfig


class TinymceEditorConfig(AppConfig):
    name = 'tinymceeditor'
