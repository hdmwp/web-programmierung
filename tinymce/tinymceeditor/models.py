from django.db import models

# Create your models here.

class Note(models.Model):
    note = models.TextField('Note', max_length=1000)
    created_at = models.DateTimeField('date created', auto_now_add=True)

    def __str__(self):
        return self.note[:50]