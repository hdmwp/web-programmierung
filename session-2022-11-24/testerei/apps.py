from django.apps import AppConfig


class TestereiConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "testerei"
