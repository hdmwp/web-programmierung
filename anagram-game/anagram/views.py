from django.shortcuts import render, redirect, get_object_or_404

from random import shuffle

# Create your views here.

from . import models

def create_anagram():
    words = list(models.Word.objects.all())
    shuffle(words)
    word = words[0]
    wordstring = word.word
    anagram = list(wordstring)
    shuffle(anagram)
    return "".join(anagram), word.id

def game(request):
    context = {}
    if request.POST:
        print(request.POST["id"])
        print(request.POST["answer"])
        correct = get_object_or_404(models.Word, pk=int(request.POST["id"]))
        if correct.word == request.POST["answer"]:
            anagram, id = create_anagram()
            context = {
                "anagram": anagram,
                "id": id,
                "message": "Richtig!"
            }
            print("Korrekt!")
        else:
            print("Falsch!")
            context["anagram"] = request.POST["anagram"]
            context["id"] = request.POST["id"]
            context["message"] = "Falsch!"
        return render(request, "game/game.html", context)
    else:
        anagram, id = create_anagram()
        context = {
            "anagram": anagram,
            "id": id,
        }
    return render(request, "game/game.html", context)

  