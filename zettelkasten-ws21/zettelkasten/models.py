from django.db import models
from colorfield.fields import ColorField

# Create your models here.

class Note(models.Model):
    '''
    A note...
    '''
    title = models.CharField(max_length=200)
    # Content is the result of the WYSIWYG-Editor
    content = models.TextField()
    date_created = models.DateTimeField(auto_now_add=True)
    # Optional due date, blank required for admin interface
    date_due = models.DateField(blank=True, null=True)

    def __str__(self):
        formatted_created = self.date_created.strftime("%Y-%m-%d %H:%M:%S")
        return f"{self.title} ({formatted_created}) - Due: {self.date_due:%d.%m.%Y}"
        return f"{self.title} ({self.date_created:%Y-%m-%d %H:%M:%S}) - Due: {self.date_due}"
        return f"{self.title} ({str(self.date_created)[:19]}) - Due: {self.date_due}"

    # Alternative:
    # Set default formats in settings.py: https://docs.djangoproject.com/en/3.2/ref/settings/#date-format
    # Alternative 2:
    # Format in template: {{ date_created|date:"Y-m-d H:i:s" }}


class TagType(models.Model):
    '''
    Represents a type, e.g. person, category, subject, ...
    '''
    name = models.CharField(max_length=50)
    # Requires pip install django-colorfield!!!
    color = ColorField()
    # Prefix is used for quick entry of new tags
    # Examples: @, #, person:, ...
    prefix = models.CharField(max_length=10)

    def __str__(self):
        return self.name


class Tag(models.Model):
    '''
    Represents a tag of a certain type, e.g. person:Kai
    '''
    name = models.CharField(max_length=100)
    type = models.ForeignKey(to=TagType, on_delete=models.CASCADE, related_name="tags")
    notes = models.ManyToManyField(to=Note, related_name="tags")

    def __str__(self):
        return self.name
