from django.contrib import admin

# Register your models here.

from . import models 

admin.site.register(models.Note)
admin.site.register(models.TagType)
admin.site.register(models.Tag)
