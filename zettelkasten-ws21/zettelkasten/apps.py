from django.apps import AppConfig


class ZettelkastenConfig(AppConfig):
    name = 'zettelkasten'
