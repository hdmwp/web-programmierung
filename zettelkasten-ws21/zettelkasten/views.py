from django.shortcuts import render

# Create your views here.

from . import models


def filtered(request, tag_id):
    return index(request, tag_id)

def index(request, tag_id=None):
    context = {}
    if not tag_id:
        context["notes"] = models.Note.objects.all()
    else:
        context["notes"] = models.Note.objects.filter(tags__contains=tag_id)
    context["tagtypes"] = models.TagType.objects.all()
    return render(request, "zettelkasten/index.html", context)

def edit(request):
    return render(request, "zettelkasten/tiny.html")
