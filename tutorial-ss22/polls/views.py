from django.shortcuts import render
from django.http import HttpResponse

from . import models

# Create your views here.

def begruessung(request):
    if "name" in request.GET:
        person = models.Person()
        person.name = request.GET["name"]
        person.save()

    persons = models.Person.objects.all()
    context = {}
    context["name"] = "Kai"
    context["title"] = "Hallo Sommersemester!"
    context["persons"] = persons
    return render(request, 'polls/begruessung.html', context)