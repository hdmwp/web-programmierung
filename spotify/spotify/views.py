from django.shortcuts import render, redirect

# Create your views here.

from . import spotify, models

spotify_client = spotify.SpotifyClient()


def update_recent_tracks():
    recently_played = spotify_client.get_recently_played()
    for entry in recently_played:
        rp, created = models.Recent_Track.objects.get_or_create(played_at=entry["played_at"])
        if created:
            track, created = models.Track.objects.get_or_create(spotify_id=entry["uri"])
            if created:
                track.name = entry["name"]
                track.length = entry["length"]
                for artist_data in entry["artists"]:
                    artist, created = models.Artist.objects.get_or_create(spotify_id=artist_data["uri"])
                    if created:
                        artist.name = artist_data["name"]
                        artist.save()
                        print(f"Created new artist {artist.name}")
                    track.artists.add(artist)

                track.save()
                print(f"Created new track {track.name}")
            rp.track = track
            rp.save()
            print(f"Created new entry in recent tracks at {rp.played_at}")
    





def index(request):
    if not spotify_client.is_logged_in():
        return spotify_client.get_connect_redirect()
    update_recent_tracks()
    context = {}
    context["recent"] = models.Recent_Track.objects.all()
    return render(request, "spotify/index.html", context=context)


def spotify_callback(request):
    spotify_client.process_connect_callback(request)
    return redirect(index)


