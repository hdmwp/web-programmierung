from django.contrib import admin

# Register your models here.

from . import models

admin.site.register(models.Track)
admin.site.register(models.Recent_Track)
admin.site.register(models.Artist)
