from django.db import models

# Create your models here.

class Genres(models.Model):
    name = models.CharField(max_length=1000, default="")
    def __str__(self):
        return self.name


class Artist(models.Model):
    name = models.CharField(max_length=1000, default="")
    spotify_id = models.CharField(max_length=1000)
    genres = models.CharField(max_length=1000, default="", blank=True, null=True)
    def __str__(self):
        return self.name


class Track(models.Model):
    name = models.CharField(max_length=1000, default="")
    spotify_id = models.CharField(max_length=1000)
    length = models.IntegerField(default=0) # in ms
    artists = models.ManyToManyField(to=Artist)

    def __str__(self):
        return self.name


class Recent_Track(models.Model):
    track = models.ForeignKey(to=Track, on_delete=models.CASCADE, null=True)
    played_at = models.DateTimeField()

    def __str__(self):
        if not self.track:
            return "NO TRACK"
        return f"{self.track.name} ({self.played_at})"



