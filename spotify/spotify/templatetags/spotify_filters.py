from django import template

# https://docs.djangoproject.com/en/3.2/howto/custom-template-tags/
register = template.Library()


@register.filter
def join_artists(value):
    '''
    Expected value: [{"name": "Metallica"}, ...]
    '''
    names = [artist["name"] for artist in value]
    return ", ".join(names)

@register.filter
def spotify_open(value):
    '''
    Expected value: Either a dictionary containing a "uri" key
    or just a spotify URI string of the form "spotify:track:DKJDSKJDKJ"
    '''
    if type(value)==dict:
        uri = value["uri"]
    else:
        uri = value
    elements = uri.split(":")
    spotify_type = elements[1]
    spotify_id = elements[2]
    return f'https://open.spotify.com/{spotify_type}/{spotify_id}'
