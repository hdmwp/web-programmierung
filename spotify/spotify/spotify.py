from urllib.parse import urlencode
import requests
import base64
from datetime import datetime, timedelta
import json
from django.shortcuts import redirect

# Create credentials.py in the same folder than this file
# It needs to set client_id and client_secret
# This file is git ignored.

from . import credentials

PROVIDER_URL = "https://accounts.spotify.com/authorize"
TOKEN_URL = "https://accounts.spotify.com/api/token"
API_URL = "https://api.spotify.com/v1"
CLIENT_ID = credentials.client_id
CLIENT_SECRET = credentials.client_secret

# Evil global variable, will fix later...
connected = False
token = ""
code = ""

class SpotifyClient:
    def __init__(self):
        # The following attributes have to be adapted to the application
        # Yes, we could make this smarter, at least with default arguments in
        # the constructor, but hey, we only have one application so far.
        self.client_id = CLIENT_ID
        self.client_secret = CLIENT_SECRET
        self.redirect_uri = 'http://localhost:8000/spotify/spotify_callback'
        self.code = "" # Contains the user-specific code from Spotify
        # If the code is different from the empty string, it means the 
        # user is successfully authenicated.
        self.current_token = "" # Stores the current token
        self.token_expires_at = datetime.now() 


    def logout(self):
        '''
        Forget code and token
        '''
        self.code = ""
        self.token = ""


    def is_logged_in(self):
        '''
        If we have a code, we are "logged in".
        '''
        return self.code != ""


    def get_connect_redirect(self):
        params = urlencode({
            'client_id': CLIENT_ID,
            'scope': 'user-read-recently-played',
            'redirect_uri': self.redirect_uri,
            'response_type': 'code'
        })

        url = PROVIDER_URL + '?' + params
        print(f"Connecting to spotify via redirect.")
        return redirect(url)


    def process_connect_callback(self, request):
        if not "code" in request.GET:
            raise Exception("Not a valid callback request!")
        self.code = request.GET["code"]


    def get_token(self):
        '''
        Check if we have a valid token. Create a new token if necessary.
        '''
        if self.current_token and datetime.now() < self.token_expires_at:
            return self.current_token

        # We need a new token
        data = {
            'grant_type': 'authorization_code',
            'code': self.code,
            'redirect_uri': self.redirect_uri,
            }
        headers = {
            'Authorization': 'Basic ' + base64.b64encode(f'{CLIENT_ID}:{CLIENT_SECRET}'.encode("UTF-8")).decode("UTF-8"),
          }
        # print(f"Headers: {headers}")
        # print(f"Data: {data}")
        response = requests.post(TOKEN_URL, data = data, headers=headers)
        # print(response)
        # print(response.text)
        data = json.loads(response.text)
        if not "access_token" in data:
            raise Exception("OAUTH Error")
        # Pro-Tip: put the actual data you get in a comment 
        # as a reminder what is expected here and what is available.
        # {"access_token":"BQBQ....",
        # "token_type":"Bearer",
        # "expires_in":3600,
        # "refresh_token":"AQqqcjnO...",
        # "scope":"user-read-email user-read-recently-played"}
        self.current_token = data["access_token"]
        print(data["access_token"])
        self.token_expires_at = datetime.now() + timedelta(seconds=data["expires_in"])
        # Open question: do we need the refresh_token?
        print(f"Got a new token: {self.current_token}")
        return self.current_token

    def _get_request(self, path, params={}):
        url = path
        if path.startswith("/"):
            url = API_URL + path

        headers = {
            'Authorization': 'Bearer ' + self.get_token(),
            'Content-Type': 'application/json',
                }
        print(f"API Request to {url}")
        response = requests.get(url, headers=headers, params=params)
        return json.loads(response.text)

    def get_recently_played(self):
        # Paging support, ask for data until there is no "next"
        next_url = ""
        res = []
        while True:
            if next_url:
                data = self._get_request(next_url)
            else:
                # Strangely, some people only get 32 at most
                data = self._get_request("/me/player/recently-played", {"limit": 50})
            for item in data["items"]:
                print(item)
                # It is always a good idea to get to see the actual data
                # print(json.dumps(item, indent=2))
                track = {}
                track["name"] = item["track"]["name"]
                track["played_at"] = datetime.fromisoformat(item["played_at"].replace("Z", "+00:00"))
                track["uri"] = item["track"]["uri"]
                track["length"] = item["track"]["duration_ms"]
                track["artists"] = []
                for artist in item["track"]["artists"]:
                    track["artists"].append({
                        "name": artist["name"],
                        "uri": artist["uri"],
                            })
                res.append(track)
            print(f"Paging next: {data['next']}")
            print(f"Items: {len(data['items'])}")
            if "next" in data and data["next"] is not None:
                next_url = data["next"]
            else:
                break
        # "total" does not exist, despite the docs
        # print(f"Total: {len(data['total'])}")
        # print(json.dumps(data, indent=2))
        return res

    def get_artist(self, artist_uri):
        data = self._get_request(artist_uri)

        return data

        


