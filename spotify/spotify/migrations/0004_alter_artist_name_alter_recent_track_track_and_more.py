# Generated by Django 4.0.3 on 2022-04-28 10:59

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('spotify', '0003_alter_track_length'),
    ]

    operations = [
        migrations.AlterField(
            model_name='artist',
            name='name',
            field=models.CharField(default='', max_length=1000),
        ),
        migrations.AlterField(
            model_name='recent_track',
            name='track',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='spotify.track'),
        ),
        migrations.AlterField(
            model_name='track',
            name='length',
            field=models.IntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='track',
            name='name',
            field=models.CharField(default='', max_length=1000),
        ),
    ]
