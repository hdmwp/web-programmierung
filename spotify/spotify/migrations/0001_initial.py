# Generated by Django 4.0.3 on 2022-04-28 10:27

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Track',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=1000)),
                ('spotify_id', models.CharField(max_length=1000)),
                ('length', models.FloatField()),
            ],
        ),
        migrations.CreateModel(
            name='Recent_Track',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('played_at', models.DateTimeField()),
                ('track', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='spotify.track')),
            ],
        ),
        migrations.CreateModel(
            name='Artist',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=1000)),
                ('spotify_id', models.CharField(max_length=1000)),
                ('tracks', models.ManyToManyField(to='spotify.track')),
            ],
        ),
    ]
