from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render, reverse
from .models import Todo, Category


def overview(request):
    todos = Todo.objects.all()
    return render(request, 'todo_list/overview.html', {"todos": todos, "status_choices": Todo.status_choices, "categories": Category.objects.all()})


def detail(request, todo_id):
    todo = get_object_or_404(Todo, pk=todo_id)
    return render(request, 'todo_list/detail.html', {'todo': todo})


def status_change(request, todo_id):
    todo = get_object_or_404(Todo, pk=todo_id)
    todo.status = request.POST["status"]
    todo.save()
    return HttpResponseRedirect(reverse('todo_list:overview'))


def new(request):
    name = request.POST["name"]
    description = request.POST["description"]
    status = request.POST["status"]
    deadline = request.POST["deadline"]
    category = Category.objects.get(pk=request.POST["category"])

    Todo.objects.create(name=name,
                        description=description,
                        status=status,
                        deadline=deadline,
                        category=category)
    return HttpResponseRedirect(reverse('todo_list:overview'))
