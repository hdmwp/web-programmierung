from django.contrib import admin
from .models import Category, Todo


class TodoInline(admin.TabularInline):
    model = Todo


class CategoryAdmin(admin.ModelAdmin):
    inlines = [TodoInline]


admin.site.register(Category, CategoryAdmin)
admin.site.register(Todo)
