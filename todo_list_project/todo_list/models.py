from django.db import models


class Category(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return f"{self.name}"


class Todo(models.Model):
    status_choices = [
        ("D", "Done"),
        ("T", "Todo"),
        ("C", "Canceled")
    ]

    name = models.CharField(max_length=100)
    description = models.CharField(max_length=1000)
    deadline = models.DateTimeField()
    status = models.CharField(max_length=1, choices=status_choices, default="T")
    category = models.ForeignKey(Category, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.name} {self.category} {self.status} {self.deadline}"
