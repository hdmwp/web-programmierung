"""project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path

from . import views

urlpatterns = [
    path("", views.index, name="index"),
    path("user_add", views.user_add, name="user_add"),
    path("user_store", views.user_store, name="user_store"),
    path("user_detail/<int:id>", views.user_detail, name="user_detail"),
    path("user_delete/<int:id>", views.user_delete, name="user_delete"),
    path("payment_add", views.payment_add, name="payment_add"),
    path("payment_store", views.payment_store, name="payment_store"),
    path("payment_detail", views.payment_detail, name="payment_detail"),
    path("payment_edit", views.payment_edit, name="payment_edit"),
    path("payment_edit_store", views.payment_edit_store, name="payment_edit_store"),
    path("balance", views.balance, name="balance"),
]
