from django.shortcuts import render, redirect, get_object_or_404

# Create your views here.


from . import models

def index(request):
    context = {}
    context["users"] = models.User.objects.all()
    context["payments"] = models.Payment.objects.all()
    context["overall_payment_amount"] = models.get_overall_payment_amount()
    context["who_is_next"] = models.who_is_next()
    return render(request, "payback/index.html", context)


def user_add(request):
    context = {}
    return render(request, "payback/user_add.html", context=context)


def user_store(request):
    if "name" in request.POST:
        models.User.objects.create(name=request.POST.get("name"))
        return redirect("index")
    else:
        print("Not a POST request")
        return redirect("index")


def user_detail(request, id):
    context = {}
    context["user"] = get_object_or_404(models.User, pk=id)
    context["payments"] = models.Payment.objects.filter(from_user=id)

    if len(context["payments"]) > 0 or len(context["user"].payment_for_user_set.all()) > 0:
        context['deleteable'] = False
    else:
        context['deleteable'] = True

    return render(request, "payback/user_detail.html", context)


def user_delete(request, id):
    user = get_object_or_404(models.User, pk=id)
    if len(user.payment_set.all()) == 0 and len(user.payment_for_user_set.all()) == 0:
        user.delete()

    return redirect("index")

def payment_add(request):
    pass


def payment_store(request):
    pass


def payment_detail(request):
    pass


def payment_edit(request):
    pass


def payment_edit_store(request):
    pass


def balance(request):
    pass


