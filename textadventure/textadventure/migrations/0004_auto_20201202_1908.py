# Generated by Django 3.0.3 on 2020-12-02 18:08

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('textadventure', '0003_auto_20201202_1855'),
    ]

    operations = [
        migrations.AlterField(
            model_name='choice',
            name='take_item',
            field=models.ForeignKey(default=None, null=True, on_delete=django.db.models.deletion.SET_NULL, to='textadventure.Item'),
        ),
    ]
