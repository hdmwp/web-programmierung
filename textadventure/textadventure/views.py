from django.shortcuts import render, get_object_or_404, redirect

# Create your views here.

from . import models

print("Los geht es hier: http://localhost:8000/textadventure/start")

def station(request, name="start"):
    '''
    Hier wird die passende Station geholt und ausgegeben.
    Wird kein Name übergeben, dann wird die Station 'start' geholt.
    '''

    players = models.Player.objects.all()
    if not players:
        player = models.Player()
        player.name="Default Player"
        player.save()
    else:
        player = players[0]

    station = get_object_or_404(models.Station, name=name)

    if "choice" in request.POST:
        # KAMPFSYSTEM
        if request.POST["choice"].startswith("ATTACK#"):
            enemy_id = request.POST["choice"].split("#")[1]
            enemy = get_object_or_404(models.Enemy, pk=enemy_id)
            damage = player.damage
            armour = player.armour
            # TODO: Calculate modifiers from items
            enemy.lp = enemy.lp + min(0, -damage+enemy.armour)
            print(f"Neue LP für {enemy.name}: {enemy.lp}")
            enemy.save()
            if enemy.lp > 0:
                player.lp = player.lp + min(0, -enemy.damage + armour)
                print(f"Neue LP für {player.name}: {player.lp}")
                player.save()
            return redirect("station", station)
            


        # Normale Choices aus der Datenbank
        else:
            choice = models.Choice.objects.filter(pk=request.POST["choice"])[0]
            if choice.take_item:
                choice.take_item.take()
            return redirect("station", choice.next_station)

    filtered_choices = []
    for choice in station.choice_set.all():
        # Alle Choices, die unabhängig von Items sind oder für die wir alle Items besitzen
        # Kudos to Tobias Malmsheimer für diese geniale Formulierung ;-)
        if not choice.item_set.filter(owned=False):
            filtered_choices.append(choice)
    for enemy in station.enemy_set.filter(lp__gt=0):
        filtered_choices.append({
            "text": f"{enemy.name} angreifen",
            "next_station": {"name": station.name },
            "id": f"ATTACK#{enemy.id}",
            }) 
    return render(request, "textadventure/station.html", {"station": station,
                                                          "choices": filtered_choices})
