from django.shortcuts import render, redirect

# Create your views here.

from . import models




def index(request):
    context = {}
    context["items"] = models.Item.objects.all()

    return render(request, "todo/index.html", context)


def add(request):
    if "text" in request.POST:
        # Erste Kategorie holen
        # Warnung: schlägt fehl, wenn die Datenbank noch leer ist
        category = models.Category.objects.all()[0]
        # Neues Item anlegen
        item = models.Item()
        # Item korrekt befüllen
        item.text = request.POST["text"]
        item.category = category
        # Item abspeichern
        item.save()
    return redirect('index')
