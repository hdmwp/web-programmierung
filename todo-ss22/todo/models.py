from django.db import models

# Create your models here.


status_choices = [
        ("T", "Todo"),
        ("D", "Done"),
        ("C", "Canceled"),
        ]

class Category(models.Model):
    name = models.CharField(max_length=100)


    def __str__(self):
        return self.name

class Item(models.Model):
    text = models.CharField(max_length=200)
    status = models.CharField(max_length=1, choices=status_choices, default="T")
    category = models.ForeignKey(Category, on_delete=models.CASCADE)

    def translation(self):
        status_texts = {
                "T": "Todo",
                "D": "Erledigt",
                "C": "Gecancelt",
                }
        return status_texts[self.status]

    def __str__(self):
        return f"{self.text}, Category: {self.category} ({self.status[0]})"


