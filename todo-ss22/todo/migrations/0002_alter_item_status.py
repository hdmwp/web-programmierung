# Generated by Django 4.0.3 on 2022-03-31 10:17

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('todo', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='item',
            name='status',
            field=models.CharField(choices=[('T', 'Todo'), ('D', 'Done'), ('C', 'Canceled')], default='T', max_length=1),
        ),
    ]
