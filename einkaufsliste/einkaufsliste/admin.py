from django.contrib import admin

# Register your models here.

from . import models

admin.site.register(models.Eintrag)
admin.site.register(models.Liste)
admin.site.register(models.Geschaeft)
