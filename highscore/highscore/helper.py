
def get_first_n(board):
    return board.score_set.order_by(board.get_order_by())[:board.places]
