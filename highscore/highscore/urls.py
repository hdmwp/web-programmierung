"""server URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

from . import views

urlpatterns = [
        path('', views.index, name='index'),
        path('view/<int:board_id>/<str:password>', views.view_board, name='view'),
        path('check-edit-password/<int:board_id>/<str:password>', views.check_edit_password, name='check_edit_password'),
        path('create/', views.create_edit_board, name='create'),
        path('edit/<int:board_id>/<str:edit_password>', views.create_edit_board, name='edit'),
        path('editinfo/<int:board_id>/<str:edit_password>', views.show_edit_info, name='editinfo'),
        path('apiinfo/<int:board_id>/<str:password>', views.show_api_info, name='apiinfo'),
        path('submit/<int:board_id>/<str:password>', views.submit_score, name='submit'),
        path('api/v1/boards/<int:board_id>/<str:password>', views.api_v1_boards, name='api_v1_boards'),
]
