from django.contrib import admin

# Register your models here.

from . import models

admin.site.register(models.Leaderboard)
admin.site.register(models.Score)
admin.site.register(models.WebHook)
