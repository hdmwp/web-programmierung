# Generated by Django 3.2.9 on 2021-12-01 11:45

from django.db import migrations, models
import highscore.models


class Migration(migrations.Migration):

    dependencies = [
        ('highscore', '0002_alter_leaderboard_url'),
    ]

    operations = [
        migrations.AlterField(
            model_name='leaderboard',
            name='comment',
            field=models.TextField(blank=True),
        ),
        migrations.AlterField(
            model_name='leaderboard',
            name='edit_password',
            field=models.CharField(default=highscore.models.random_string, max_length=200),
        ),
        migrations.AlterField(
            model_name='leaderboard',
            name='password',
            field=models.CharField(default=highscore.models.random_string, max_length=200),
        ),
        migrations.AlterField(
            model_name='leaderboard',
            name='short_description',
            field=models.TextField(blank=True),
        ),
    ]
