import secrets
import string

from django.db import models

# Create your models here.

SUBMISSION_TYPES = (
    ("API", "Submitted via API"),
    ("WEB", "Submitted manually via Web-Frontend"),
    ("ADMIN", "Submitted manually via Admin-Interface"),
)

RANK_BY = (
    ("SCORE", "Rank by score"),
    ("TIME", "Rank by time"),
)

ORDER = (
    ("DESC", "Sort by descending value"),
    ("ASC", "Sort by ascending value"),
)


def random_string():
    chars = string.ascii_letters + string.digits
    return ''.join(secrets.choice(chars) for i in range(10))


class Leaderboard(models.Model):
    # Usually the name of the game, ideally with a version indication.
    title = models.CharField(max_length=200)
    # Can be used to define a specific version of the game.
    short_description = models.TextField(blank=True)
    # Further information that is not necessarily shown.
    comment = models.TextField(blank=True)
    # URL where the game can be downloaded or played, e.g. Gitlab link (to specific version!)
    url = models.URLField(blank=True)
    # The password is part of all URLs and prevents guessing of board URLs.
    password = models.CharField(max_length=200, default=random_string)
    # The edit password is required to edit a board
    edit_password = models.CharField(max_length=200, default=random_string)
    # Configuration of the board (sorting and number of places)
    rank_by = models.CharField(max_length=20, choices=RANK_BY, default="SCORE")
    order = models.CharField(max_length=20, choices=ORDER, default="DESC")
    places = models.IntegerField(default=10)
    created = models.DateTimeField(auto_now_add=True)

    def get_order_by(self):
        order_by = ""
        if self.order == "DESC":
            order_by = "-"
        if self.rank_by == "SCORE":
            order_by += "score"
        else:
            order_by += "time"
        return order_by

    def __str__(self):
        return self.title


class Score(models.Model):
    board = models.ForeignKey(to=Leaderboard, on_delete=models.CASCADE)
    submission_type = models.CharField(choices=SUBMISSION_TYPES, max_length=20, default="ADMIN")
    # We support anonymous submission
    name = models.CharField(max_length=200, blank=True)
    # The score.
    score = models.IntegerField(default=0)
    # If time is important, e.g. for speedruns
    time = models.FloatField(default=0)
    # For analysis, i.e. to store arbitrary JSON data
    data = models.TextField(blank=True)
    created = models.DateTimeField(auto_now_add=True)


class WebHook(models.Model):
    board = models.ManyToManyField(Leaderboard)
    id = models.IntegerField(primary_key=True)
    token = models.CharField(max_length=70)
    description = models.CharField(max_length=200, blank=True)
    enabled = models.BooleanField(default=True)

    def __str__(self):
        return self.description
