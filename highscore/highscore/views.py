import json

from django.http import JsonResponse, HttpResponse
from django.shortcuts import render, get_object_or_404, reverse, redirect
from django.views.decorators.csrf import csrf_exempt

from highscore.helper import get_first_n
from highscore.webhooks import post_to_discord
from . import models


# Create your views here.


def index(request):
    context = {}
    context["boards"] = models.Leaderboard.objects.all()
    return render(request, "highscore/index.html", context)


def view_board(request, board_id, password):
    context = {}
    board = get_object_or_404(models.Leaderboard, id=board_id, password=password)
    context["board"] = board
    context["scores"] = board.score_set.order_by(board.get_order_by())[:board.places]
    return render(request, "highscore/view.html", context)


def check_edit_password(request, board_id, password):
    return HttpResponse("Not yet implemented.")


def create_edit_board(request, board_id=None, edit_password=None):
    context = {}
    if board_id:
        # We want to edit an existing board
        board = get_object_or_404(models.Leaderboard, id=board_id, edit_password=edit_password)
        context["board"] = board
        # The action is used to send the form with the proper parameters
        context["action"] = reverse("edit", args=(board.id, board.password))
    else:
        # We want to create a new board
        context["board"] = models.Leaderboard()
        context["action"] = reverse("create")
    if request.POST:
        # We want to save the board
        board = context["board"]
        board.title = request.POST["title"].strip()
        board.short_description = request.POST["short_description"].strip()
        board.save()
        return redirect("view", board.id, board.password)
    return render(request, "highscore/create_edit.html", context)


def show_edit_info(request, board_id, edit_password):
    return HttpResponse("Not yet implemented.")


def show_api_info(request, board_id, password):
    return HttpResponse("Not yet implemented.")


def submit_score(request, board_id, password):
    return HttpResponse("Not yet implemented.")


@csrf_exempt
def api_v1_boards(request, board_id, password):
    board = get_object_or_404(models.Leaderboard, id=board_id, password=password)
    if request.method == "POST":
        # A new score is submitted
        payload = json.loads(request.body)
        score = models.Score()
        score.board = board
        if "name" in payload:
            score.name = payload["name"]
        if "time" in payload:
            score.time = payload["time"]
        if "score" in payload:
            score.score = payload["score"]
        score.submission_type = "API"
        score.save()
        response = {"result": "ok"}

        # post to discord if score is in top n
        post_to_discord(board, score.id)
        return JsonResponse(response)
    else:
        # GET request, we return the current highscores
        response = {
            "title": board.title,
            "description": board.short_description,
            "created": board.created,
            "scores": [],
        }
        for score in get_first_n(board):
            response["scores"].append({
                "name": score.name,
                "score": score.score,
                "time": score.time,
                "created": score.created,
            })
        return JsonResponse(response)
