from django.apps import AppConfig


class HighscoreConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'highscore'
