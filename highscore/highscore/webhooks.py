from discord import Webhook, RequestsWebhookAdapter, Embed

from highscore.helper import get_first_n
from highscore.models import WebHook


def post_to_discord(board, score_id):
    board_scores = get_first_n(board)
    for rank, score in enumerate(board_scores):
        if score_id == score.id:
            embed = Embed()
            embed.title = f"New Highscore: {score.score}  :trophy:"
            embed.add_field(name="Player", value=score.name, inline=True)
            embed.add_field(name="Board", value=board.title, inline=True)
            embed.add_field(name="Rank", value=str(rank + 1), inline=True)

            hooks = WebHook.objects.filter(board=board, enabled=True)
            for hook in hooks:
                print(hook.id, hook.token)
                webhook = Webhook.partial(hook.id, hook.token, adapter=RequestsWebhookAdapter())
                webhook.send(embeds=[embed], username='Highscores',
                             avatar_url="https://img.icons8.com/color-glass/48/000000/leaderboard.png")
