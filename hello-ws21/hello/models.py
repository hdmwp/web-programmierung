from django.db import models

# Create your models here.

class Produkt(models.Model):
    menge = models.IntegerField()
    preis = models.FloatField(null=True)
    name = models.TextField()
    gekauft = models.BooleanField(default=False)

    def __str__(self):
        return f"Produkt: {self.name}, {self.preis}, {self.menge}"

    def kaufen(self):
        self.gekauft = True

